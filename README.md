# lsxo r337885 #

**lsxo**: _ls_ with _libxo_ support.  

[FreeBSD](https://www.freebsd.org/) provides the
[libxo](https://libxo.readthedocs.io/en/latest/) library in BASE so some
[utility](https://wiki.freebsd.org/LibXo) can display output in human and
machine readable formats.
The [ls](http://man.freebsd.org/ls/1) utility was converted to use _libxo_
([R284198](https://svnweb.freebsd.org/base?view=revision&revision=284198)), but
currently the support has been deleted, however the repository still stores
the last revision with _libxo_ in the history, so some user may take advantage
of this feature. This project provides the last _ls_ with _libxo_ merged with
the _ls_ in BASE.


[Repository](https://svnweb.freebsd.org/base/head/bin/ls/) history:

 - [ ] update Makefile.depend [355617](https://svnweb.freebsd.org/base?view=revision&revision=355617)
 - [ ] add Makefile.depend.options [355616](https://svnweb.freebsd.org/base?view=revision&revision=355616)
 - [ ] delete RELEASE\_CRUNCH [349990](https://svnweb.freebsd.org/base?view=revision&revision=349990)
 - [ ] do\_color\_\*  [338028](https://svnweb.freebsd.org/base?view=revision&revision=338028)
 - [ ] opts like GNU ls [338027](https://svnweb.freebsd.org/base?view=revision&revision=338027)
 - [ ] add --color=when [337956](https://svnweb.freebsd.org/base?view=revision&revision=337956)
 - [X] fix color env var [337885](https://svnweb.freebsd.org/base?view=revision&revision=337885) **<-- lsxo**
 - [X] colors with COLORTERM [337506](https://svnweb.freebsd.org/base?view=revision&revision=337506)
 - [ ] delete libxo [328100](https://svnweb.freebsd.org/base?view=revision&revision=328100)
 - [X] last revision with libxo [326025](https://svnweb.freebsd.org/base?view=revision&revision=326025)  
       % svn checkout https://svn.freebsd.org/base/head/bin/ls@326025


Getting Started

	%> git clone https://gitlab.com/alfix/lsxo.git
	%> cd lsxo
	%> make
	%> ./lsxo

Examples

	%> lsxo
	hello.c     hello.h        Makefile
	%> lsxo --libxo=xml,pretty
	<file-information version="1">
	  <directory>
	    <entry>
	      <name>hello.c</name>
	    </entry>
	    <entry>
	      <name>hello.h</name>
	    </entry>
	    <entry>
	      <name>Makefile</name>
	    </entry>
	  </directory>
	</file-information>
	%> % lsxo --libxo=json,pretty
	{
	  "__version": "1", 
	  "file-information": {
	    "directory": [
	      {
		"entry": [
		  {
		    "name": "hello.c"
		  },
		  {
		    "name": "hello.h"
		  },
		  {
		    "name": "Makefile"
		  }
		]
	      }
	    ]
	  }
	}
	%>



