#	@(#)Makefile	8.1 (Berkeley) 6/2/93
# $FreeBSD$

#.include <src.opts.mk>

#PACKAGE=runtime
PROG=	lsxo
SRCS=	cmp.c ls.c print.c util.c
MAN=	${PROG}.1

LDFLAGS= -lxo -lutil
#.if !defined(RELEASE_CRUNCH) && \
#	${MK_LS_COLORS} != no
CFLAGS+= -DCOLORLS
LDFLAGS+= -ltermcapw
#.endif

MK_DEBUG_FILES= no

PREFIX?=        /usr/local
MANDIR=		/man/man
DESTDIR=	${PREFIX}
BINDIR=		/bin

CLEANFILES=	*~ *.core

RM = rm -f
unistall:
	${RM} ${DESTDIR}${BINDIR}/${PROG}
	${RM} ${PREFIX}${MANDIR}1/${MAN}.gz

#HAS_TESTS=
#SUBDIR.${MK_TESTS}+= tests

.include <bsd.prog.mk>
